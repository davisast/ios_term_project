///
//  ViewController.swift
//  diet_app
//
//  Created by davis street on 3/15/22.
//

import UIKit

class TimelineViewController: UIViewController
{
    // outlet boilerplate
    @IBOutlet var timelineTable: UITableView!
    @IBOutlet weak var timelineObject: TimelineViewController!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        timelineTable.delegate = self
        timelineTable.dataSource = self
    }

}

extension TimelineViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("i was interacted with")
    }
}

extension TimelineViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
}
