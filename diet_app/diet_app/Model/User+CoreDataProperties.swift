//
//  User+CoreDataProperties.swift
//  diet_app
//
//  Created by Grant Davis on 4/1/22.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var userName: String?
    @NSManaged public var userAge: Int64
    @NSManaged public var userHeight: String?
    @NSManaged public var userSex: String?
    @NSManaged public var userWeight: Int64

}

extension User : Identifiable {

}
