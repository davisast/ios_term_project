//
//  Date+CoreDataProperties.swift
//  diet_app
//
//  Created by Grant Davis on 4/1/22.
//
//

import Foundation
import CoreData


extension Date {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Date> {
        return NSFetchRequest<Date>(entityName: "Date")
    }


}

extension Date : Identifiable {

}
