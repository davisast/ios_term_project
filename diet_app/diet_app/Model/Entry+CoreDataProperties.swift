//
//  Entry+CoreDataProperties.swift
//  diet_app
//
//  Created by Grant Davis on 4/1/22.
//
//

import Foundation
import CoreData


extension Entry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Entry> {
        return NSFetchRequest<Entry>(entityName: "Entry")
    }

    @NSManaged public var entryTime: String?
    @NSManaged public var entryText: String?
    @NSManaged public var hasA: Entry?

}

extension Entry : Identifiable {

}
