//
//  DiaryViewController.swift
//  diet_app
//
//  Created by davis street on 3/27/22.
//
import UIKit

class DiaryViewController: UIViewController
{

    //Labels for customization
    @IBOutlet weak var cals: UILabel!
    @IBOutlet weak var carbs: UILabel!
    @IBOutlet weak var sugar: UILabel!
    @IBOutlet weak var fiber: UILabel!
    @IBOutlet weak var protein: UILabel!
    @IBOutlet weak var water: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var sbut: UIButton!
    
    //Text fields for user input
    @IBOutlet weak var calst: UITextField!
    @IBOutlet weak var carbst: UITextField!
    @IBOutlet weak var sugart: UITextField!
    @IBOutlet weak var fibert: UITextField!
    @IBOutlet weak var proteint: UITextField!
    @IBOutlet weak var watert: UITextField!
    @IBOutlet weak var gender: UITextField!
    @IBOutlet weak var weight: UITextField!
    
    let dte = Date()
    let dteFormatter = DateFormatter()
    
    let avgMcal = 2500
    let avgFcal = 2000
    let avgMcarb = 406
    let avgFcarb = 325
    let avgMsugar = 36
    let avgFsugar = 25
    let avgMfibre = 38
    let avgFfibre = 25
    
    let avgMwater = 4
    let avgFwater = 3
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        cals.layer.masksToBounds = true
        cals.layer.cornerRadius = 5
        carbs.layer.masksToBounds = true
        carbs.layer.cornerRadius = 5
        sugar.layer.masksToBounds = true
        sugar.layer.cornerRadius = 5
        fiber.layer.masksToBounds = true
        fiber.layer.cornerRadius = 5
        protein.layer.masksToBounds = true
        protein.layer.cornerRadius = 5
        water.layer.masksToBounds = true
        water.layer.cornerRadius = 5
        date.layer.masksToBounds = true
        date.layer.cornerRadius = 5
        
        dteFormatter.dateFormat = "MM/dd/YYYY"
        date.text = dteFormatter.string(from: dte)
        
        
    }
    
    @IBAction func checkInput(_ sender: Any) {
        
        var avgMpro: Double? = Double(weight.text!)
        avgMpro = avgMpro! / 2.2046 * 0.8
        var avgFpro: Double? = Double(weight.text!)
        avgFpro = avgFpro! / 2.2046 * 0.8
        let ccal: Int? = Int(calst.text!)
        let ccarb: Int? = Int(carbst.text!)
        let csug: Int? = Int(sugart.text!)
        let cfibre: Int? = Int(fibert.text!)
        let cpro: Double? = Double(proteint.text!)
        let cwater: Int? = Int(watert.text!)
        
        if (gender.text == "M" && ccal! >= avgMcal && ccarb! >= avgMcarb && csug! >= avgMsugar && cfibre! >= avgMfibre && cpro! >= avgMpro! && cwater! >= avgMwater) {
            
            cals.text = "Success"
            
        }
        
        else {
            
            cals.text = "Failure"
            
        }
        
    }


}
